const mongoose = require('mongoose');

const anotacion = new mongoose.Schema(
    {
        titulo:       String,
        texto:        String,
        modificacion: {type: Date, default: Date.now},
        etiquetas:    [String],
    },
    {
        collection: 'anotaciones',
        timestamps: {
            updatedAt: 'modificacion',
        },
    }
);

module.exports = mongoose.model('Anotacion', anotacion);
