const express = require('express');
const router = express.Router();

router.get('/', function(req, res, next) {
  res.status(200).redirect('anotacion');
});

module.exports = router;
