const express = require('express');
const router = express.Router();

const modelAnotacion = require('../models/anotacion');
const Anotacion = modelAnotacion.model('Anotacion');

router.get('/', function(req, res, next) {
  Anotacion.find({}, {}, (err, results) => {
    if (err) {
      res.status(500).send(err.message);
    }

    res.status(200).render('anotaciones', {anotaciones: results});
  });
});

router.get('/nueva', function(req, res, next) {
  res.status(200).render('anotacion', {anotacion: null});
});

router.post('/', (req, res, next) => {
  (new Anotacion(req.body)).save((err, response) => {
    if (err) {
      res.status(500).send(err.message);
    }

    res.status(200).redirect('/');
  });
});

router.get('/editar/:id', function(req, res, next) {
  Anotacion.find({_id: req.params.id}, {}, (err, results) => {
    if (err) {
      res.status(500).send(err.message);
    }

    res.status(200).render('anotacion', {anotacion: results[0]});
  });
});

router.put('/', (req, res, next) => {
  Anotacion.updateOne({_id: req.body._id}, req.body, (err, response) => {
    if (err) {
      res.status(500).send(err.message);
    }

    res.status(200).redirect('/');
  });
});

module.exports = router;
